---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.10.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
import numpy as np
import matplotlib.pyplot as plt
```

# A notebook illustrating the problems with git and notebooks

I'll use this to illustrate the basic problem.

```python
x = np.linspace(0,100,103)
y = x**3
plt.plot(x,y)
```

```python

```
